#include <stdio.h>
#include <string.h>

void print_str(const char *str)
{
    printf("%s\n", str);
}

int main()
{
    const char *str = "Hello world";
    const char *str1 = "Hello";
    const char *str2 = "world";
    char target[20];

    memset(target, '\0', sizeof(target) / sizeof(target[0]));

    /* Problem 1 */
    printf("Hello world\n");

    /* Problem 2 */
    printf("%s\n", str);

    /* Problem 3 */
    sprintf(target, "%s %s", str1, str2);
    printf("%s\n", target);

    /* Problem 4 */
    print_str(str);

    return 0;
}